package com.vendavo
import com.atlassian.jira.rest.client.api.domain.BasicIssue
import com.atlassian.jira.rest.client.api.domain.Issue
import spock.lang.Specification
/**
 * Created with IntelliJ IDEA.
 * User: Marek Chowaniok
 * Date: 15/11/13
 */
class VendavoJiraTest extends  Specification{

    VendavoJira vendavoJira

    def setup() {
        def patchTrain = "7.6.x"
        vendavoJira = new VendavoJira()
        vendavoJira.setPatchTrain(patchTrain)
    }

    def cleanup() {
        vendavoJira.deleteAllCreatedIssues()
//        vendavoJira.getCreatedIssues().each {vendavoJira.getClient().getIssueClient().deleteIssue(it.getKey(),false) }
    }

    def 'create subtask'() {
        when:
        vendavoJira.setQuery("key = 'VEN-3'")
        vendavoJira.getIssuesFromQuery()

        then:
        assert vendavoJira.getCreatedIssues().size() == 1
        vendavoJira.getCreatedIssues().each {
            BasicIssue basicIssue = (BasicIssue) it;
            Issue issue = vendavoJira.getClient().getIssueClient().getIssue(basicIssue.getKey()).claim();
            assert issue.getSummary().contains("Port to")
        }

    }


    def 'create subtasks'() {
        when:
        vendavoJira.setQuery("key in ('VEN-3', 'VEN-4')")
        vendavoJira.getIssuesFromQuery()

        then:
        assert vendavoJira.getCreatedIssues().size() == 2
        vendavoJira.getCreatedIssues().each {
            BasicIssue basicIssue = (BasicIssue) it;
            Issue issue = vendavoJira.getClient().getIssueClient().getIssue(basicIssue.getKey()).claim();
            assert issue.getSummary().contains("Port to")
        }

    }
    
    




}
