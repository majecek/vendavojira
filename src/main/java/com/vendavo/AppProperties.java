package com.vendavo;

import java.io.IOException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Chowaniok
 * Date: 14/11/13
 */
public class AppProperties {

    private static final AppProperties instance = new AppProperties();

    private final Properties appProps = new Properties();

    public static AppProperties getInstance() {
        return instance;
    }

    private AppProperties() {
        try {
            appProps.load(getClass().getClassLoader().getResourceAsStream("app.properties"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getJiraURL() {
        return appProps.getProperty("jira.url");
    }

    public String getJiraUser() {
        return appProps.getProperty("jira.user");
    }

    public String getJiraPassword() {
        return appProps.getProperty("jira.pwd");
    }
}
