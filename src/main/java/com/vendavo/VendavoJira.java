package com.vendavo;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.RestClientException;
import com.atlassian.jira.rest.client.api.domain.BasicIssue;
import com.atlassian.jira.rest.client.api.domain.EntityHelper;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.IssueType;
import com.atlassian.jira.rest.client.api.domain.Project;
import com.atlassian.jira.rest.client.api.domain.SearchResult;
import com.atlassian.jira.rest.client.api.domain.input.ComplexIssueInputFieldValue;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;
import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@lombok.Data
public class VendavoJira {

    private String patchTrain;
	private String query;
	private String url;
	private String username;
	private String password;
    private JiraRestClient client;
    private List<BasicIssue> createdIssues = Lists.newArrayList();
    final List<Promise<BasicIssue>> promises = Lists.newArrayList();
    final Logger logger = LoggerFactory.getLogger(this.getClass());
    final AsynchronousJiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();


    /**
     * Create subtask for each found issue for given query and generate.
     * At the end we generate JQL with all subtasks and print it into console
     */
    public void getIssuesFromQuery(){
        try {
            setProperties();
        } catch (IOException e) {
            logger.error(Arrays.toString(e.getStackTrace()));
        }

        if (getClient() == null) {
            setClient(factory.createWithBasicHttpAuthentication(URI.create(getUrl()), getUsername(), getPassword()));
        }
        SearchResult searchResult = client.getSearchClient().searchJql(getQuery()).claim();
        try {
            for (BasicIssue basicIssue: searchResult.getIssues()) {
                createSubtask(basicIssue);
            }
            transformPromissToBasicIssue(promises);
        } catch (RestClientException e) {
            logger.error("Created issues:" + getCreatedIssues());
            logger.error(Arrays.toString(e.getStackTrace()));
        }

        generateJQL();
    }

    private void generateJQL() {
        StringBuilder builder = new StringBuilder();
        builder.append("JQL: key in ( ");
        for (BasicIssue basicIssue : getCreatedIssues()) {
            builder.append("'").append(basicIssue.getKey()).append("', ");
        }
        builder.deleteCharAt(builder.length()-2).append(")");
        logger.info(builder.toString());
    }

    private void setProperties() throws IOException {
        while (Strings.isNullOrEmpty(getQuery())) {
            System.out.print("Enter your JQL query (i.e. key = 'ven-1234'): ");
            setQuery((new BufferedReader(new InputStreamReader(System.in, "UTF-8"))).readLine().replaceAll("\"", "'"));
        }

        while (Strings.isNullOrEmpty(getPatchTrain())) {
            System.out.print("Enter Patch Train (i.e. 8.1.x): ");
            setPatchTrain((new BufferedReader(new InputStreamReader(System.in, "UTF-8"))).readLine().replaceAll("\"","'"));
        }

        if (Strings.isNullOrEmpty(AppProperties.getInstance().getJiraPassword()) ||  Strings.isNullOrEmpty(AppProperties.getInstance().getJiraUser())) {
            logger.error("Add jira username and password into resources/app.properties file !");
            throw new RuntimeException();
        }
        setPassword(AppProperties.getInstance().getJiraPassword());
        setUsername(AppProperties.getInstance().getJiraUser());
        setUrl(AppProperties.getInstance().getJiraURL());
    }

    private void createSubtask(BasicIssue basicIssue) {

        final IssueRestClient issueClient = client.getIssueClient();
        final Issue issueParent =   client.getIssueClient().getIssue(basicIssue.getKey()).claim();
        final Project project =     client.getProjectClient().getProject(issueParent.getProject().getKey()).claim();
        final IssueType issueType = EntityHelper.findEntityByName(project.getIssueTypes(), "Sub-task");

        String summaryString = generateSummary(issueParent);

        final IssueInputBuilder issueInputBuilder = new IssueInputBuilder(project, issueType)
                .setDescription("See the parent task for description.")
                .setSummary(summaryString)
                .setComponents(issueParent.getComponents().iterator().next())
                .setFieldValue(issueParent.getFieldByName("Patch Train").getId(), ComplexIssueInputFieldValue.with("value", getPatchTrain()))
                .setFieldValue(issueParent.getFieldByName("Severity").getId(), ComplexIssueInputFieldValue.with("value", getSeverity(issueParent)))
                .setFieldValue("parent", ComplexIssueInputFieldValue.with("key", issueParent.getKey()));

        promises.add(issueClient.createIssue(issueInputBuilder.build()));
    }

    private String getSeverity(Issue issueParent) {
        String severity = "Normal";
        JSONObject jsonObject = (JSONObject) issueParent.getFieldByName("Severity").getValue();
        try {
            severity = (String) jsonObject.get("value");
        } catch (JSONException e) {
            logger.error(Arrays.toString(e.getStackTrace()));
        }
        return severity;
    }

    private String generateSummary(Issue issueParent) {
        String summaryString = issueParent.getSummary();
        if (summaryString.length() > 240) {
            summaryString = summaryString.substring(0, 240);
        }
        summaryString = "Port to "+ getPatchTrain() +": " + summaryString;
        return summaryString;
    }

    private void deleteAllCreatedIssues() {
        Iterator<BasicIssue> basicIssuesIter = getCreatedIssues().iterator();
        while (basicIssuesIter.hasNext()) {
            BasicIssue basicIssue = basicIssuesIter.next();
            client.getIssueClient().deleteIssue(basicIssue.getKey(),false);
            basicIssuesIter.remove();
        }
    }

    private void transformPromissToBasicIssue(List<Promise<BasicIssue>> promises) {
        createdIssues = Lists.transform(promises, new Function<Promise<BasicIssue>, BasicIssue>() {
            @Override
            public BasicIssue apply(Promise<BasicIssue> promise) {
                return promise.claim();
            }
        });
    }


    public static void main(String[] args) {
        new VendavoJira().getIssuesFromQuery();
//        System.exit(1);
    }


}
